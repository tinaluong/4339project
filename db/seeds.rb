# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Car Data
Car.delete_all
cars = Car.create ([
                      {model: 'Sedan Lexus IS 250', color: 'Starfire Pearl', vin:'2B4JB25Z92K361520', retailprice: '33650'},
                      {model: 'Sedan Lexus IS 250', color: 'Nebula Gray Pearl', vin:'2G1WB55K779047529', retailprice: '34650'},
                      {model: 'Sedan Lexus IS 250', color: 'Silver Lining Metallic', vin:'1HGCG6574YA335495', retailprice: '36085'},
                      {model: 'Sedan Lexus IS 250', color: 'Atomic Silver', vin:'YV1SW61N612029622', retailprice: '34650'},
                      {model: 'Sedan Lexus IS 250', color: 'Black', vin:'1GC2CXCG6BZ427828', retailprice: '33500'},
                      {model: 'Sedan Lexus IS 250', color: 'Red', vin:'1GTEK24C99Z992888', retailprice: '37500'},
                      {model: 'Sedan Lexus IS 250', color: 'Deep Sea Mica', vin:'2T1CF28P72C198156', retailprice: '33750'},
                      {model: 'Coupe Lexus RC 350', color: 'Starfire Pearl', vin:'1FMEU73E99U151640', retailprice: '38790'},
                      {model: 'Coupe Lexus RC 350', color: 'Nebula Gray Pearl', vin:'1B3EL36X13N763916', retailprice: '39500'},
                      {model: 'Coupe Lexus RC 350', color: 'Silver Lining Metallic', vin:'4F2CZ94184K929163', retailprice: '41050'},
                      {model: 'Coupe Lexus RC 350', color: 'Atomic Silver', vin:'1GCHC23U62F368977', retailprice: '42000'},
                      {model: 'Coupe Lexus RC 350', color: 'Black', vin:'1FMYU95HX5K238937', retailprice: '41500'},
                      {model: 'Coupe Lexus RC 350', color: 'Infrared', vin:'1GYEE23AX80106744', retailprice: '43070'},
                      {model: 'SUV Lexus RX 350', color: 'Nebula Gray Pearl', vin:'YV1SW612862126785', retailprice: '37970'},
                      {model: 'SUV Lexus RX 350', color: 'Silver Lining Metallic', vin:'1GTCS1942Y8539879', retailprice: '38950'},
                      {model: 'SUV Lexus RX 350', color: 'Black', vin:'WAUHF78P67A613477', retailprice: '40070'},
                      {model: 'SUV Lexus RX 350', color: 'Deep Sea Mica', vin:'1D3HB13P89J511580', retailprice: '40000'},
                      {model: 'SUV Lexus RX 350', color: 'Fire Agate Pearl', vin:'1GCRKPE30BZ898997', retailprice: '41050'},
                      {model: 'SUV Lexus RX 350', color: 'Satin Cashmere Metallic', vin:'5TDZY68A58S863448', retailprice: '43710'},
                      {model: 'SUV Lexus RX 350', color: 'Starfire Pearl', vin:'ZFFAA54A460144789', retailprice: '42550'}
                  ])


#Customer Data
Customer.delete_all
customers = Customer.create ([
                                {last_name: 'Mckenzie', first_name: 'Sheila', address: '8505 Scott St. Houston, TX 77085', phone: '(713) 984-4759', email: 'sheila.mckenzie@gmail.com'},
                                {last_name: 'Meyers', first_name: 'Noelle', address: '5562 Sed Ave. Houston, TX 85903', phone: '(832) 484-3530', email: 'noelle.meyers@gmail.com'},
                                {last_name: 'Smith', first_name: 'Jenna', address: '1946 Malesuada Avenue Houston, TX 86049', phone: '(281) 584-9036', email: 'jana.nunez@gmail.com'},
                                {last_name: 'Daniel', first_name: 'Xerxes', address: '688 Dairy Ashford St. Houston, TX 77072', phone: '(713) 556-0115', email: 'xerxes.daniel@gmail.com'},
                                {last_name: 'Johnston', first_name: 'Eleanor', address: '8985 Lobortis Ave. Sugarland, 65904', phone: '(713) 416-6910', email: 'eleanor.johnston@gmail.com'},
                                {last_name: 'Dejesus', first_name: 'Tasha', address: '5354 Bellaire Blvd. Houston, TX 77072', phone: '(832) 486-0947', email: 'tasha.dejesus@gmail.com'},
                                {last_name: 'Clayton', first_name: 'Arsenio', address: '5763 Venenatis St. Houston, TX 85904', phone: '(832) 210-2782', email: 'arsenio.clayton@gmail.com'},
                                {last_name: 'Tyson', first_name: 'Bianca', address: '1176 Bibendum Rd. Apt. 204 Houston, TX 89045', phone: '(281) 436-2887', email: 'bianca.tyson@gmail.com'},
                                {last_name: 'Cote', first_name: 'Diana', address: '9641 Torquent St. Sugarland, 77304', phone: '(310) 980-5019', email: 'diana.cote@gmail.com'},
                                {last_name: 'Flores', first_name: 'Richard', address: '2483 Augue. St. Apt. A Houston, TX 56043', phone: '(713) 340-7737', email: 'richard.flores@gmail.com'},
                                {last_name: 'Donovan', first_name: 'Lisandra', address: '8453 Leo Ave. Houston, TX 74059', phone: '(832) 365-3459', email: 'lisandra.donovan@gmail.com'},
                                {last_name: 'Ingram', first_name: 'Colette', address: '6539 Cullen St. Houston, TX 77008', phone: '(713) 163-4587', email: 'colette.ingram@gmail.com'},
                                {last_name: 'Smith', first_name: 'Blaze', address: '6614 Neque Rd. Houston, TX 90754', phone: '(832) 746-8072', email: 'blaze.smith@gmail.com'},
                                {last_name: 'Rowland', first_name: 'Shelby', address: '3058 Kirkwood St. Houston, TX 77059', phone: '(281) 276-9493', email: 'shelby.rowland@gmail.com'},
                                {last_name: 'Efron', first_name: 'David', address: '12110 Neff St. Houston, TX 77075', phone: '(713) 185-4625', email: 'efron.david@gmail.com'},
                                {last_name: 'Hahn', first_name: 'Brandon', address: '1309 Est Rd. Sugarland, 75603', phone: '(832) 791-5214', email: 'brandon.hahn@gmail.com'},
                                {last_name: 'Rush', first_name: 'Hammett', address: '4966 Synott St. Houston, TX 77049', phone: '(832) 247-7334', email: 'hammett.rush@gmail.com'},
                                {last_name: 'Ortiz', first_name: 'Nicholas', address: '7325 Magna Ave. Houston, TX 77049', phone: '(713) 602-9807', email: 'nicholas.ortiz@gmail.com'},
                                {last_name: 'Yang', first_name: 'Helen', address: '83940 Calhound St. Houston, TX 79048', phone: '(281) 366-7467', email: 'helen.yang@gmail.com'}
                            ])

#Salesperson Data
Salesperson.delete_all
salespeople = Salesperson.create ([  {last_name: 'Luong', first_name: 'Tina', phone: '(713) 815-4996'},
                                     {last_name: 'Kent', first_name: 'Issac', phone: '(832) 668-6845'},
                                     {last_name: 'Lee', first_name: 'Troy', phone: '(832) 384-9045'},
                                     {last_name: 'Johnston', first_name: 'Steven', phone: '(713) 486-0934'},
                                     {last_name: 'Lindsey', first_name: 'Norris', phone: '(832) 332-6910'}
                                 ])

#Quote Data
Quote.delete_all

#Loan Data
Loan.delete_all