class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.decimal :wholesale_price
      t.decimal :markup_price
      t.decimal :sales_tax
      t.decimal :final_price
      t.boolean :salestatus, default: false
      t.integer :car_id
      t.integer :customer_id
      t.integer :salesperson_id

      t.timestamps null: false
    end
  end
end
