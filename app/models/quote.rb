class Quote < ActiveRecord::Base
  belongs_to :customer
  belongs_to :salesperson
  belongs_to :car
  has_one :loan

  validates_presence_of :salesperson_id
  validates_presence_of :customer_id
  validates_presence_of :car_id

  after_save :update_car_status
  before_save :calculate_final_price


  def update_car_status
    if self.salestatus == true
      Car.update(self.car_id, :carstatus => true)
    else
      Car.update(self.car_id, :carstatus => false)
    end

  end

  before_destroy { |record| Car.update(self.car_id, :carstatus => false)}

  def quote_info
    "#{car.model}, #{car.color}, $#{final_price.round(2)}"
  end

  def boolean_name
    salestatus ? 'Sold' : 'Unsold'
  end


  def calculate_final_price
  self.sales_tax = (self.car.markupprice * 0.043).round(2)
  self.final_price = (self.car.markupprice + self.sales_tax).round(2)
  end

end
