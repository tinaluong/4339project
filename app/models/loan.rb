class Loan < ActiveRecord::Base
  belongs_to :quote
  validates_presence_of :quote_id
  validates_presence_of :down_payment
  validates_presence_of :interest_rate
  #before_save :calculate_monthly_payment

  def self.calculate_loan_table(loan_amount = 0, term = 0,interest_rate = 0)
    Rails.logger.debug "Inputs: #{loan_amount} #{term} #{interest_rate}"
    payment = []
    months = (term * 12) - 1
    loan_amount = BigDecimal(loan_amount)
    interest_rate = BigDecimal(interest_rate)
    rate = interest_rate/1200
    n = rate * loan_amount
    d = 1 - (1 + rate)**-(term*12)
    monthly_payment = (n / d).round(2)
    count = 0
    (0..months).each do
      payment << count+= 1
      payment << monthly_payment
      monthly_interest = (loan_amount * rate).round(2)
      payment << monthly_interest
      monthly_principal = (monthly_payment - monthly_interest).round(2)
      payment << monthly_principal
      balance  = loan_amount -= monthly_principal
      payment << balance.round(2)
    end
    return payment
  end

  def calculate_loan_table
    return self.class.calculate_loan_table(self.loan_amount,self.term,self.interest_rate)
  end


  private
  def calculate_monthly_payment
    unless self.down_payment.blank?
      self.loan_amount = (self.quote.final_price - self.down_payment).round(2)
      r = self.interest_rate / 1200
      n = r * self.loan_amount
      d = 1 - (1 + r)**-((self.loan_term.term * 12))
      self.monthly_payment = (n / d).round(2)
    end
  end
end
