class Customer < ActiveRecord::Base
  has_many :quotes
  validates_presence_of :last_name
  validates_presence_of :first_name
  validates_presence_of :phone

  def full_name
    "#{last_name}, #{first_name}"
  end

  def self.search(search)
    if search
      where('last_name LIKE ?', "%#{search}%")
    else
      scoped
    end
  end
end
