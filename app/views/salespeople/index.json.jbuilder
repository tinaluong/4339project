json.array!(@salespeople) do |salesperson|
  json.extract! salesperson, :id, :last_name, :first_name, :phone
  json.url salesperson_url(salesperson, format: :json)
end
