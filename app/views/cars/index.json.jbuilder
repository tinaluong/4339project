json.array!(@cars) do |car|
  json.extract! car, :id, :model, :color, :vin, :retailprice, :markupprice, :carstatus
  json.url car_url(car, format: :json)
end
