pdf.text "Southwest Lexus Dealership", :align => :center, :size => 25, :style => :bold
pdf.text "Sales Report", :align => :center, :size => 20, :style => :bold

pdf.move_down(20)

pdf.text "*********************************************************************"

pdf.move_down(10)

pdf.text "Gross Revenue: #{number_to_currency(@quotes.where(:salestatus => true).sum(:final_price))}",
:size => 13, :style => :bold
pdf.text "Net Profit: #{number_to_currency((@quotes.where(:salestatus => true).sum(:final_price)) -
        (@quotes.where(:salestatus => true).sum(:wholesale_price)))}", :size => 13, :style => :bold
pdf.text "Total Sales Tax: #{number_to_currency(@quotes.where(:salestatus => true).sum(:sales_tax))}",
:size => 13, :style => :bold

pdf.move_down(10)

pdf.text "*********************************************************************"
pdf.move_down(10)

pdf.text "List of Sold Cars", :size => 13, :style => :bold
pdf.move_down(5)
reports = [["Salesperson", "Car", "Retail Price", "Markup Price", "Sales Tax", "Total Price"]]
reports += @quotes.map do |report|
    [
        report.salesperson.full_name,
        report.car.car_details,
        number_to_currency(report.wholesale_price),
        number_to_currency(report.markup_price),
        number_to_currency(report.sales_tax),
        number_to_currency(report.final_price)
    ]
end

pdf.table(reports, :header => true, :row_colors => ["D2E1A9","F2EEE0"], :cell_style => { size: 10 })
