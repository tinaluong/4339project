json.array!(@loans) do |loan|
  json.extract! loan, :id, :down_payment, :loan_amount, :term, :interest_rate, :monthly_payment, :quote_id
  json.url loan_url(loan, format: :json)
end
