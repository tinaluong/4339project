json.array!(@quotes) do |quote|
  json.extract! quote, :id, :wholesale_price, :markup_price, :sales_tax, :final_price, :salestatus, :car_id, :customer_id, :salesperson_id
  json.url quote_url(quote, format: :json)
end
