# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


updateMarkup = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/markupprice', {},(json, response) ->
    retailPrice = json['retailprice']
    $('#quote_wholesale_price').val(retailPrice)
    markupPrice = (retailPrice * 1.1).toFixed(2)
    #getMarkupprice = json['markupprice']
    #getMarkupprice.toFixed(2)
    $('#quote_markup_price').val(markupPrice)

    salestax= (markupPrice * 0.043).toFixed(2)
    $('#quote_sales_tax').val(salestax)
    total= (parseFloat(markupPrice) + parseFloat(salestax)).toFixed(2)
    $('#quote_final_price').val(total)

$ ->
  $('#quote_car_id').change -> updateMarkup()

